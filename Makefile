export DOCKER_IMAGE_NAME ?= registry.gitlab.com/guardianproject-ops/docker-alpine-git:latest

-include $(shell curl -sSL -o .build-harness-ext "https://go.sr2.uk/build-harness"; echo .build-harness-ext)

export README_TEMPLATE_FILE := ${BUILD_HARNESS_EXTENSIONS_PATH}/templates/README_gp.md.gotmpl

## Push $DOCKER_IMAGE_NAME
docker/push:
	$(call assert-set,DOCKER)
	$(call assert-set,DOCKER_IMAGE_NAME)
	"$(DOCKER)" push $(DOCKER_IMAGE_NAME)
