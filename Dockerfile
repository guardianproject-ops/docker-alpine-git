FROM docker.io/library/docker:git
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

ENV PYTHONUNBUFFERED=1
RUN set -ex; \
    apk add --no-cache --update \
      make \
      curl \
      bash \
      jq \
      python3 \
      py3-pip \
      py3-click \
      py3-requests \
      py3-dateutil;
